let app = getApp();
//替换成开发者后台设置的安全域名
let url = "http://60.190.29.198:9082/api";
//let url = "http://localhost:8080/jh/api";

//let url = "http://abcde.vaiwan.com";
//若要在测试应用中临时使用类似abcdef.vaiwan.com 的二级域名代理到无公网IP的服务端开发环境，
//请参考内网穿透工具介绍:
//https://open-doc.dingtalk.com/microapp/debug/ucof2g


Page({
    data:{
        authCode:'',
        openId:'',
        nick:'',
        cardNo:'',
        custName:'',
        fid:'',
        _userId:'',
        _userName:'',
        jobnumber:'',
        tel:''
    },
    onLoad(){
        let _this = this;

        dd.getAuthCode({
            success:(res)=>{
                _this.setData({
                    authCode:res.authCode
                })
             
               /** dd.httpRequest({
                    url: url+'/login',
                    method: 'POST',
                    data: {
                        authCode: res.authCode
                    },
                    dataType: 'json',
                    success: function(res) {
                        let userInfo = res.data.result;
                        _this.setData({
                            openId:userInfo.openid,
                            nick:userInfo.nick

                        })
                    },
                    fail: function(res) {
                        dd.alert({content: JSON.stringify(res)});
                    },
                    complete: function(res) {
                        dd.hideLoading();
                    }
                    
                });  **/
                dd.httpRequest({
                    url: url+'/server.wxapp.WxAppAction$findCustInfoByOpenId.json',
                    method: 'POST',
                    data: {
                        openId: 'onkxmwkDHxgGrs7f90rj5P2tdAgk'
                                            },
                    dataType: 'json',
                    success: function(res) {
                        let userInfo = res.data;
                        _this.setData({
                            cardNo:userInfo.cardNo,
                            custName:userInfo.custName

                        })
                    },
                    fail: function(res) {
                        dd.alert({content: JSON.stringify(res)});
                    },
                    complete: function(res) {
                        dd.hideLoading();
                    }
                    
                });
            },
            fail: (err)=>{
                dd.alert({
                    content: JSON.stringify(err)
                })
            }
        })
        
    },
     onSetOpenId(event){     
       let _this = this;      
        dd.httpRequest({
                    url: url+'/server.wxapp.WxAppAction$findCustInfoByOpenId.json',
                    method: 'POST',
                    data: {
                        openId: 'onkxmwkDHxgGrs7f90rj5P2tdAgk'
                                            },
                    dataType: 'json',
                    success: function(res) {
                        let userInfo = res.data;
                        _this.setData({
                            fid:userInfo.custId,
                            openId:userInfo.openId

                        })
                    },
                    fail: function(res) {
                        dd.alert({content: JSON.stringify(res)});
                    },
                    complete: function(res) {
                        dd.hideLoading();
                    }
                    
                });
          
         
    },
    onGetDingInfo(event){ 
       let _this = this;      
        dd.httpRequest({
                    url: url+'/server.dingtalk.DingtalkAppAction$findUserInfo.json',
                    method: 'POST',
                    data: {
                        authCode: this.data.authCode
                    },
                    dataType: 'json',
                    success: function(res) {
                        let userInfo = res.data;
                        _this.setData({
                            _userId:userInfo.userid,
                            _userName:userInfo.name,
                            jobnumber:userInfo.jobnumber,
                            tel:userInfo.mobile
                        })
                    },
                    fail: function(res) {
                        dd.alert({content: JSON.stringify(res)});
                    },
                    complete: function(res) {
                        dd.hideLoading();
                    }
                    
                });
    }
})